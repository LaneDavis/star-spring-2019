﻿using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using UnityEngine;

public class Explosion : MonoBehaviour {
    
    [Header("Damage")]
    public float DamageAmount = 20f;
    public AnimationCurve DamageFallOffByRadius;

    [Header("Explosive Force")]
    public float Force;
    public float ExplosiveRadius;

    private const float Duration = 0.1f;
    private float timer;

    private void Update() {
        timer += Time.deltaTime;
        if (timer > Duration) {
            Destroy(gameObject);
        }
    }
    
    public void OnCollisionEnter (Collision other) {
        if (other.gameObject.layer >= 12) {
            DamageReceiver damageReceiver = other.transform.root.GetComponent<DamageReceiver>();
            if (damageReceiver == null) return;
            Vector3 dif = transform.position - damageReceiver.transform.position;
            damageReceiver.TakeDamage(DamageAmount * DamageFallOffByRadius.Evaluate(dif.magnitude / (transform.localScale.x / 2f)));
            Rigidbody rb = damageReceiver.GetComponent<Rigidbody>();
            if (rb != null) {
                rb.AddExplosionForce(Force, transform.position - Vector3.up, ExplosiveRadius);
            }
        }
    }
    
}
