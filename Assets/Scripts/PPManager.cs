﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Serialization;

public class PPManager : MonoBehaviour {

    public static PPManager Instance;

    public enum Volumes {
        Default,
        Death,
        Flashbang
    }

    public List<PPVolumeRef> Vols = new List<PPVolumeRef>();
    private List<PPFactor> ActiveFactors = new List<PPFactor>();

    public PPFactor DefaultFactor;

    private void Awake() {
        Instance = this;
        AddFactor(DefaultFactor);
    }

    public void AddFactor(PPFactor factor) {
        ActiveFactors.Add(new PPFactor(factor));
    }

    public void RemoveFactor(PPFactor factor) {
        foreach (PPFactor f in ActiveFactors) {
            if (f.Name == factor.Name) f.StartFading(); 
        }
    }

    private void Update() {

        // Set each PostProcessVolume's weight to be the max among all factors.
        foreach (PPVolumeRef vol in Vols) {
            List<PPFactor> factors = FactorsOfVol(vol);
            if (factors.Count == 0) vol.Ref.weight = 0f;
            else vol.Ref.weight = factors.Max(e => e.CurrentValue);
        }
        
        // Age factors and remove outdated ones.
        for (int i = ActiveFactors.Count - 1; i >= 0; i--) {
            ActiveFactors[i].IncrementAge(Time.deltaTime);
            if (ActiveFactors[i].ShouldBeDeleted) ActiveFactors.RemoveAt(i);
        }
        
    }

    private List<PPFactor> FactorsOfVol(PPVolumeRef vol) { return ActiveFactors.Where(e => e.Volume == vol.Volume).ToList(); }

}

[System.Serializable]
public class PPVolumeRef {

    public PPManager.Volumes Volume;
    public PostProcessVolume Ref;

}

[System.Serializable]
public class PPFactor {

    public PPManager.Volumes Volume;
    public bool IsSteadyFactor;

    [Header("Steady Factors")]
    public string Name;
    public float Value;
    public float DecayRate;
    private bool isFading;
    private float curValue;
    
    [Header("Non-Steady Factors")]
    public AnimationCurve Curve;
    private float timer;

    public PPFactor (PPFactor factor) {
        Volume = factor.Volume;
        IsSteadyFactor = factor.IsSteadyFactor;
        Name = factor.Name;
        Value = factor.Value;
        curValue = factor.Value;
        DecayRate = factor.DecayRate;
        Curve = new AnimationCurve();
        foreach (Keyframe kf in factor.Curve.keys) {
            Curve.AddKey(new Keyframe(kf.time, kf.value, kf.inTangent, kf.outTangent));
        }
    }
    
    public bool ShouldBeDeleted { get {
        if (IsSteadyFactor) return curValue < 0.001f;
        return timer > Curve[Curve.keys.Length - 1].time;
    } }

    public float CurrentValue { get {
        if (IsSteadyFactor) return curValue;
        return Curve.Evaluate(timer);
    } }

    public void StartFading() {
        if (IsSteadyFactor) isFading = true;
    }

    public void IncrementAge (float time) {
        if (IsSteadyFactor) curValue = MathUtilities.DecayToward(curValue, isFading ? 0f : Value, time, DecayRate);
        timer += time;
    }
    
   

}