﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour {

    [HideInInspector] public float UnspentDamage;
    
    public void TakeDamage(float damageAmount) {
        UnspentDamage += Mathf.Max(damageAmount, 0f);
    }
    
}
