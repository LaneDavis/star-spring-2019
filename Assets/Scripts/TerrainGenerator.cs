﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun.Demo.Procedural;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TerrainGenerator : MonoBehaviour {

    public GameObject Ground;
    public GameObject ObjectPrefab;
    public Transform ObjectFolder;
    
    public List<GameObject> ActiveObjects = new List<GameObject>();
    
    [Header("Object Style")]
    public float SizeBase;
    public float SizeVariance;
    public float HeightBase;
    public float HeightVariance;
    public bool CanGenerateOnOtherObjects;

    [Header("Box Generation")]
    public Vector2 BoxSize = new Vector2(200f, 200f);
    public int BoxObjects = 100;

    [Header("Brush Generation")]
    public TerrainBrush TerrainBrush;
    [HideInInspector] public float BrushRadius;
    [HideInInspector] public int BrushObjects;
    
    public LayerMask raycastMask;
    
    public void GenerateObjectsInBox() {

        int objectsGenerated = 0;
        int safety = 0;
        while (objectsGenerated < BoxObjects && safety < 1000) {
            RaycastHit hit;
            safety++;
            if (Physics.Raycast(transform.position + new Vector3(Random.Range(-0.5f * BoxSize.x, 0.5f * BoxSize.x), 100f, Random.Range(-0.5f * BoxSize.y, 0.5f * BoxSize.y)), Vector3.down, out hit, 1000f, raycastMask)) {
                if (!CanGenerateOnOtherObjects && hit.collider.gameObject != Ground) continue;
                GameObject newObject = Instantiate(ObjectPrefab, hit.point, Quaternion.identity, ObjectFolder);
                RandomizeObject(newObject);
                ActiveObjects.Add(newObject);
                objectsGenerated++;
            }
        }
        
    }

    public void GenerateObjectsAtBrush() {

        int objectsGenerated = 0;
        int safety = 0;
        while (objectsGenerated < BrushObjects && safety < 1000) {
            RaycastHit hit;
            safety++;
            float dist = Mathf.Pow(Random.Range(0f, 1f), 2f) * BrushRadius;
            Vector2 circlePoint = Random.insideUnitCircle.normalized * dist;
            Vector3 startPoint = new Vector3(circlePoint.x, 100f, circlePoint.y);
            if (Physics.Raycast(TerrainBrush.transform.position + startPoint, Vector3.down, out hit, 1000f, raycastMask)) {
                if (!CanGenerateOnOtherObjects && hit.collider.gameObject != Ground) continue;
                GameObject newObject = Instantiate(ObjectPrefab, hit.point, Quaternion.identity, ObjectFolder);
                RandomizeObject(newObject);
                ActiveObjects.Add(newObject);
                objectsGenerated++;
            }
        }

    }

    private void RandomizeObject(GameObject newObject) {
        
        // Randomize Size
        float scale = SizeBase + Random.Range(-SizeVariance, SizeVariance);
        newObject.transform.localScale = new Vector3(scale, scale, scale);
                
        // Randomize Height
        float height = HeightBase + Random.Range(-HeightVariance, HeightVariance);
        newObject.transform.Translate(Vector3.up * height);
                
        // Randomize Rotation
        newObject.transform.Rotate(0f, Random.Range(0f, 360f), 0f);
        
    }

    public void ClearObjects() {
        foreach (GameObject obj in ActiveObjects) {
            if (obj != null) {
                DestroyImmediate(obj);
            }
        }
        
        ActiveObjects.Clear();
    }

    public void ClearEmptyObjectEntries() {
        for (int i = ActiveObjects.Count - 1; i >= 0; i--) {
            if (ActiveObjects[i] == null) {
                ActiveObjects.RemoveAt(i);
            }
        }
    }

}

#if UNITY_EDITOR
[CustomEditor (typeof(TerrainGenerator))]
public class TerrainGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        TerrainGenerator script = (TerrainGenerator)target;
        
        if (GUILayout.Button("Generate Objects In Box")) {
            script.GenerateObjectsInBox();
        }

        if (GUILayout.Button("Clear Empty Object Entries")) {
            script.ClearEmptyObjectEntries();
        }

        if (GUILayout.Button("Clear Objects")) {
            script.ClearObjects();
        }
    }
    
}
#endif