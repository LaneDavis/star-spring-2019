﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class ResourceSpawner : MonoBehaviour {

    public LayerMask RaycastMask;
    
    [Header("Resources")]
    public GameObject WoodPrefab;
    public float WoodWeight;
    public GameObject StonePrefab;
    public float StoneWeight;
    public GameObject CrystalPrefab;
    public float CrystalWeight;

    [Header("Spawn Rate")]
    public float SecondsPerSpawn;
    public int ResourceCap;
    private float timer;

    [Header("Spawn Zone")]
    public float SpawnRadius;

    private List<GameObject> activeResources = new List<GameObject>();

    private void Update() {
        timer += Time.deltaTime;
        if (timer > SecondsPerSpawn && activeResources.Count < ResourceCap) {
            SpawnResource();
        }
    }
    
    private void SpawnResource() {
        Vector3 pos = transform.position + Random.insideUnitSphere * SpawnRadius;
        RaycastHit hit;
        bool didHit = Physics.Raycast(new Vector3(pos.x, 100f, pos.z), Vector3.down, out hit, 1000f, RaycastMask);
        if (didHit) {
            GameObject newResource = Instantiate(RandomResourcePrefab(), hit.point + Vector3.up * 50f, Quaternion.identity, transform);
            activeResources.Add(newResource);
        }
    }

    private GameObject RandomResourcePrefab() {
        float random = Random.Range(0f, WoodWeight + StoneWeight + CrystalWeight);
        if (random < WoodWeight) return WoodPrefab;
        else if (random < WoodWeight + StoneWeight) return StonePrefab;
        else return CrystalPrefab;
    }

}
