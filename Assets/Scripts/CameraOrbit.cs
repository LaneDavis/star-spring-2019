﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : MonoBehaviour {
    private Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;
    private Vector2 md;
       
    GameObject character;
    Transform cameraTransform;
    [HideInInspector] public PlayerController PlayerController;

    public void Init(GameObject newCharacter, PlayerController playerController) {
   
           character = newCharacter;
           PlayerController = playerController;

    }
   
       // Update is called once per frame
       void Update() {
   
           md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
   
           md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
           smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
           smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
           mouseLook += smoothV;
           mouseLook.y = Mathf.Clamp(mouseLook.y + smoothV.y, -85f, 85f);
   
           transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
           character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);

           // Camera.main.transform.localEulerAngles = new Vector3(Mathf.Clamp(Camera.main.transform.localEulerAngles.x, -60, 60), 0, 0); 

    }
       
}
