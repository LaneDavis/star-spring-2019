﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions {

    public static void StrikeWhere<T>(this List<T> list, Func<T, bool> where) {
        for (int i = list.Count - 1; i >= 0; i--) {
            if (where(list[i])) {
                list.RemoveAt(i);
            }
        }
    }

    public static T RandomElement<T>(this List<T> list) {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static void TriggerAll(this List<FxPackageController> list, GameObject anchor = null, float power = 1f) {
        list.ForEach(e => e.TriggerAll(anchor, power));
    }
    
}
