﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ValleyWallGenerator : MonoBehaviour {

    public GameObject ObjectPrefab;
    public Transform ObjectFolder;
    public Vector2 GroundSize = new Vector2(200f, 200f);
    public float DistanceFuzz;
    public List<GameObject> ActiveObjects = new List<GameObject>();
    public float SizeBase;
    public float SizeVariance;
    public float HeightBase;
    public float HeightVariance;

    public int numObjects = 100;
    
    public LayerMask raycastMask;
    
    public void GenerateObjects() {

        foreach (GameObject rock in ActiveObjects) {
            if (rock != null) {
                DestroyImmediate(rock);
            }
        }
        
        ActiveObjects.Clear();
        
        for (int i = 0; i < numObjects; i++) {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 1000f, raycastMask)) {
                GameObject newObject = Instantiate(ObjectPrefab, hit.point, Quaternion.identity, ObjectFolder);

                // Move to Random Point on the Outer Wall
                if (Random.Range(0f, 1f) > 0.5f) {
                    float xPos = hit.point.x + 0.5f * Mathf.Sign(Random.Range(-1f, 1f)) * (GroundSize.x + Random.Range(0f, DistanceFuzz));
                    float zPos = hit.point.z + 0.5f * Random.Range(-1f, 1f) * (GroundSize.y + DistanceFuzz);
                    newObject.transform.position = new Vector3(xPos, newObject.transform.position.y, zPos);
                }
                else {
                    float xPos = hit.point.x + 0.5f * Random.Range(-1f, 1f) * (GroundSize.x + DistanceFuzz);
                    float zPos = hit.point.z + 0.5f * Mathf.Sign(Random.Range(-1f, 1f)) * (GroundSize.y + Random.Range(0f, DistanceFuzz));
                    newObject.transform.position = new Vector3(xPos, newObject.transform.position.y, zPos);
                }
                
                // Randomize Size
                float scale = SizeBase + Random.Range(-SizeVariance, SizeVariance);
                newObject.transform.localScale = new Vector3(scale, scale, scale);
                
                // Randomize Height
                float height = HeightBase + Random.Range(-HeightVariance, HeightVariance);
                newObject.transform.Translate(Vector3.up * height);
                
                // Randomize Rotation
                newObject.transform.Rotate(0f, Random.Range(0f, 360f), 0f);
                
                ActiveObjects.Add(newObject);
            }
        }
        
    }

}

#if UNITY_EDITOR
[CustomEditor (typeof(ValleyWallGenerator))]
public class ValleyWallGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        ValleyWallGenerator script = (ValleyWallGenerator)target;
        
        if (GUILayout.Button("Generate Objects")) {
            script.GenerateObjects();
        }
    }
    
}
#endif