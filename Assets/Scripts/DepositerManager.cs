﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepositerManager : MonoBehaviour {
    
    public static DepositerManager Instance;
    [HideInInspector] public List<Depositer> Depositers = new List<Depositer>();
    
    private void Awake () {
        Instance = this;
    }
    
    public void RegisterDepositer (Depositer newDepositer) {
        if (!Depositers.Contains(newDepositer)) {
            Depositers.Add(newDepositer);
        }
    }
    
}
