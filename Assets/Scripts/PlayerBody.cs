﻿using UnityEngine;
using Photon.Pun;
using Rewired;

public class PlayerBody : MonoBehaviourPunCallbacks {

    [HideInInspector] public PlayerController PlayerController;
    [HideInInspector] public Gun ActiveGun;
    public DamageReceiver DamageReceiver;

    public Player RewiredPlayer;
    
    // Start is called before the first frame update
    public float jumpforce = 7;
    public float JumpCooldown = 1f;
    private float jumpTimer;

    [HideInInspector] public Rigidbody rb;

    public float speed;
    public float sprintMultiplier;

    [Header("Ground Detection")]
    public LayerMask raycastMask;
    public float raycastDistance = 1f;
    private bool isGrounded;

    [Header("Fall Damage")]
    public float FallImmunityAtStart = 4f;
    public AnimationCurve FallDamageBySpeed;
    private float lastFallSpeed;

    [Header("Dash")]
    public float DashCooldown = 1f;
    private float dashTimer;
    public float DashDecayRate = 13f;
    public float DashPower = 5f;
    private Vector3 dashVec;

    private float lifeTimer;
    private bool isDead;

    [Header("Gun Swapping")]
    public float GunSwapCooldown = 0.5f;
    private float GunSwapTimer = Mathf.Infinity;

    void Start() {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    // Update is called once per frame
    void FixedUpdate() {

        if (RewiredPlayer == null) {
            RewiredPlayer = ReInput.players.GetPlayer(0);
        }

        if (photonView.IsMine == false && PhotonNetwork.IsConnected) {    // If this isn't the body the active player is controlling, do nothing.
            if (PlayerController.CameraRig != null) {
                Destroy(PlayerController.CameraRig);
            }
            return;
        }

        if (isDead) return;    // Don't do anything while dead.

        jumpTimer += Time.fixedDeltaTime;
        dashTimer += Time.fixedDeltaTime;
        lifeTimer += Time.fixedDeltaTime;
        
        float mH = Input.GetAxis("Horizontal");
        float mV = Input.GetAxis("Vertical");
        Vector2 groundVec = new Vector2(mH, mV);
        groundVec = MathUtilities.RotateVector2(groundVec, -transform.eulerAngles.y);

        bool issprinting = Input.GetKey(KeyCode.LeftShift);
        float speedThisFrame = speed;
        if (issprinting) {
            speedThisFrame *= sprintMultiplier;
        }
        rb.velocity = new Vector3(groundVec.x * speedThisFrame * (50f / (50f + PlayerController.SandOwned)), rb.velocity.y, groundVec.y * speedThisFrame * (50f / (50f + PlayerController.SandOwned)));
        rb.velocity += dashVec;
        dashVec = dashVec.normalized * MathUtilities.Decay(dashVec.magnitude, Time.deltaTime, DashDecayRate);

        if (Input.GetKeyDown(KeyCode.Space)) {
            if (isGrounded && jumpTimer > JumpCooldown) {
                jumpTimer = 0f;
                rb.AddForce(Vector3.up * jumpforce, ForceMode.Impulse);
            }
        }

        if (dashTimer > DashCooldown && Input.GetKeyDown(KeyCode.Q)) {
            dashTimer = 0f;
            dashVec = -transform.right * DashPower;
        } else if (dashTimer > DashCooldown && Input.GetKeyDown(KeyCode.E)) {
            dashTimer = 0f;
            dashVec = transform.right * DashPower;
        } 

        if (Input.GetKeyDown("escape"))  {
            if (Cursor.lockState == CursorLockMode.Locked) {
                Cursor.lockState = CursorLockMode.None;
            }
            else {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        isGrounded = Physics.Raycast(transform.position, Vector3.down, raycastDistance, raycastMask);
        float curFallSpeed = Mathf.Abs(rb.velocity.y);
        if (lifeTimer > FallImmunityAtStart) {
            TakeDamage(FallDamageBySpeed.Evaluate(Mathf.Abs(curFallSpeed - lastFallSpeed)));
        }
        lastFallSpeed = curFallSpeed;

        if (DamageReceiver.UnspentDamage > 0f) {
            TakeDamage(DamageReceiver.UnspentDamage);
            DamageReceiver.UnspentDamage = 0f;
        }
        
        // Do Shooting
        if (Input.GetMouseButton(0)) {
            ActiveGun.TryShoot();
        }

        // Gun Swapping
        GunSwapTimer += Time.deltaTime;
        if (RewiredPlayer.GetAxis("Swap Gun") > 0) {
            if (GunSwapTimer > GunSwapCooldown) {
                GunSwapTimer = 0f;
                PlayerController.SwapGun(1);
            }
        } else if (RewiredPlayer.GetAxis("Swap Gun") < 0) {
            if (GunSwapTimer > GunSwapCooldown) {
                GunSwapTimer = 0f;
                PlayerController.SwapGun(-1);
            }
        }
        
        // Gun Bob
        if (ActiveGun != null) {
            ActiveGun.ApplyBob(rb.velocity.magnitude);
        }

    }

    public void CollectResource(Collectible.CollectibleType type, float value) {
        PlayerController.AddCrystals(type, value);
    }

    public void DepositCrystals(float value) {
        PlayerController.DepositOverTime(value);
    }

    public void TakeDamage(float amount) {
        if (!photonView.IsMine) return;
        PlayerController.TakeDamage(amount);
    }

    public void Die() {
        isDead = true;
    }

}