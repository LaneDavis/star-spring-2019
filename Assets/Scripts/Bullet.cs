﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float MaxLife = 30f;
    private float lifeTimer;

    public float DamageAmount = 10f;

    private void Update() {
        lifeTimer += Time.deltaTime;
        if (lifeTimer > MaxLife) {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.layer == 9) {
            Destroy(gameObject);
        } else if (other.gameObject.layer == 10) {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(DamageAmount);
        }
    }
    
}
