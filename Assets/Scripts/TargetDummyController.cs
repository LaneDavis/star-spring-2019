﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDummyController : MonoBehaviour {

    public Bar HealthBar;
    public DamageReceiver DamageReceiver;

    public float MaxHP = 100f;
    private float CurHP = 100f;
    public float Regen = 5f;

    private void Update() {
        float incomingDamage = DamageReceiver.UnspentDamage;
        DamageReceiver.UnspentDamage = 0f;
        CurHP = Mathf.Clamp(CurHP + Regen * Time.deltaTime - incomingDamage, 0f, MaxHP);
        HealthBar.SetProgress(CurHP / MaxHP);
        if (Camera.main != null) {
            HealthBar.transform.parent.LookAt(Camera.main.transform);
        }
    }

}
