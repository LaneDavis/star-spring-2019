﻿using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour {

    public Image FillImage;
    public RectTransform FillRT;
    private Vector2 baseSizeDelta;
    private float maxWidth;

    public Gradient FillColor;
    
    [Header("Testing")]
    [Range(0f, 1f)] public float progress = 1f;
    private float lastProgress = 1f;

    private void Awake() {
        maxWidth = FillRT.rect.width;
        baseSizeDelta = FillRT.sizeDelta;
    }

    private void Update() {
        
        // Set Bar Size
        FillRT.sizeDelta = baseSizeDelta + new Vector2(maxWidth * (progress - 1), 0f);
        
        // Set Bar Color
        Color c = FillColor.Evaluate(progress);
        if (c.a > 0f) FillImage.color = c;
        
    }

    public void SetProgress(float newProgress) {
        progress = newProgress;
    }

}
