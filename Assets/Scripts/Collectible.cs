﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    public enum CollectibleType {
        Wood,
        Stone,
        Sand
    }

    public CollectibleType Type;
    
    public float value = 5f;

    public SoundCall CollectionSound;

    [Header("Movement")]
    public AnimationCurve FloatCurve;
    private Vector3 basePosition;
    public AnimationCurve StartScaleCurve;
    public AnimationCurve CollectionScaleCurve;
    public float CollectionDuration = 0.75f;
    private float lifeTimer;
    private float collectionTimer;
    private float baseScale;
    public float RotationSpeed = 15f;

    private bool hasBeenCollected = false;
    
    [Header("Ground Detection")]
    public LayerMask RaycastMask;
    
    [Header("Vacuum Pull")]
    public float PullDecayRate;
    private Vector3 curPullVector;

    private void Start() {
        RaycastHit hit;
        bool didHit = Physics.Raycast(transform.position, Vector3.down, out hit, 1000f, RaycastMask);
        if (!didHit) {
            Destroy(gameObject);
            return;
        }

        transform.position = hit.point;
        basePosition = transform.position;
        baseScale = transform.localScale.x;
        float scaleMag = baseScale * StartScaleCurve.Evaluate(0f);
        transform.localScale = new Vector3(scaleMag, scaleMag, scaleMag);
    }

    private void Update() {
        lifeTimer += Time.deltaTime;
        if (hasBeenCollected) {
            collectionTimer += Time.deltaTime;
            if (collectionTimer > CollectionDuration) {
                Destroy(gameObject);
                return;
            }
        }

        float scaleMag = baseScale * StartScaleCurve.Evaluate(lifeTimer) * CollectionScaleCurve.Evaluate(collectionTimer / CollectionDuration);
        transform.localScale = new Vector3(scaleMag, scaleMag, scaleMag);
        
        transform.RotateAround(Vector3.up, RotationSpeed * Mathf.Deg2Rad * Time.deltaTime);

        if (curPullVector.magnitude > 0.01f) {
            transform.Translate(curPullVector * Time.deltaTime, Space.World);
            curPullVector.x = MathUtilities.Decay(curPullVector.x, Time.deltaTime, PullDecayRate);
            curPullVector.y = MathUtilities.Decay(curPullVector.y, Time.deltaTime, PullDecayRate);
            curPullVector.z = MathUtilities.Decay(curPullVector.z, Time.deltaTime, PullDecayRate);
            basePosition = transform.position;
        }
        else {
            transform.position = basePosition + Vector3.up * FloatCurve.Evaluate(lifeTimer);
        }
        
    }
    
    public void OnCollisionEnter(Collision other) {
        if (hasBeenCollected) return;
        if (other.gameObject.tag == "Player") {
            other.gameObject.GetComponent<PlayerBody>().CollectResource(Type, value);
            SoundManager.thisSoundManager.PlaySound(CollectionSound, other.gameObject);
            hasBeenCollected = true;
        }
    }

    public void OnTriggerEnter(Collider other) {
        if (hasBeenCollected) return;
        if (other.gameObject.tag == "Player") {
            other.gameObject.GetComponent<PlayerBody>().CollectResource(Type, value);
            SoundManager.thisSoundManager.PlaySound(CollectionSound, other.gameObject);
            hasBeenCollected = true;
        }
    }

    public void ApplyPull(Vector3 targetLocation, float pullAmount) {
        curPullVector += (targetLocation - transform.position).normalized * pullAmount;
    }
    
}
