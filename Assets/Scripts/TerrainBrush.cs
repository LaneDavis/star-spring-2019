﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TerrainBrush : MonoBehaviour {

    private TerrainGenerator ParentGenerator;

    [Header("Brush Generation")]
    public float BrushRadius;
    public int BrushObjects;

    
    
    public void GenerateObjectsAtBrush() {
        if (!EnsureParent()) return;

        ParentGenerator.TerrainBrush = this;
        ParentGenerator.BrushRadius = BrushRadius;
        ParentGenerator.BrushObjects = BrushObjects;
        ParentGenerator.GenerateObjectsAtBrush();
    }

    public void ClearObjects() {
        if (!EnsureParent()) return;
        
        ParentGenerator.ClearObjects();
    }
    
    private bool EnsureParent() {
        if (transform.parent != null) {
            ParentGenerator = transform.parent.GetComponent<TerrainGenerator>();
            if (ParentGenerator == null) {
                Debug.LogError("Terrain Generator needed at parent of Terrain Brush.");
                return false;
            }
        }
        else {
            Debug.LogError("Terrain Brush must have a Terrain Generator as its parent.");
            return false;
        }

        return true;
    }
    
}

#if UNITY_EDITOR
[CustomEditor (typeof(TerrainBrush))]
public class TerrainBrushEditor : Editor {

    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        TerrainBrush script = (TerrainBrush)target;
        
        if (GUILayout.Button("Generate Objects At Brush")) {
            script.GenerateObjectsAtBrush();
        }
        
        if (GUILayout.Button("Clear Objects")) {
            script.ClearObjects();
        }
    }
    
}
#endif
