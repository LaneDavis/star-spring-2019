﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {
    public float MaxHealth = 100f;
    private float curHealth;

    public FxPackageController DamageFx;
    
    void Awake() {
        curHealth = MaxHealth;
    }
    
    public void TakeDamage(float damageAmount) {
        curHealth -= damageAmount;
        if (curHealth <= 0f) {
            Destroy(gameObject);
        }
        else {
            DamageFx.TriggerAll(damageAmount);
        }
    }
    
}
