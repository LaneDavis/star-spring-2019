﻿ using System.Collections;
using System.Collections.Generic;
 using System.Net.Mime;
 using System.Text;
 using Photon.Pun;
 using TMPro;
using UnityEngine;
 using UnityEngine.UI;
#if UNITY_EDITOR
 using UnityEditor;
#endif

 public class PlayerController : MonoBehaviourPunCallbacks {

    [HideInInspector] public GameObject PlayerObject;
    [HideInInspector] public PlayerBody Body;
    [HideInInspector] public GameObject CameraRig;
    [HideInInspector] public Transform GunAttachPoint;
    
    public float MaxHP = 100f;
    [HideInInspector] public float CurHP = 100f;

    public float ResourceCap = 100f;

    [Header("Health Bar")]
    public Bar HealthBar;
    public List<FxPackageController> DamageFX = new List<FxPackageController>();
    public AnimationCurve BarShakeByHealthLeft;
    public Image Vignette;

    // Crystal Depositing
    private const float valuePerCrystal = 100;
    private float crystalsToDeposit = 0f;

    public bool Take5Damage;

    [Header("Resources")]
    [HideInInspector] public float WoodOwned;
    [HideInInspector] public float WoodBanked;
    [HideInInspector] public float StoneOwned;
    [HideInInspector] public float StoneBanked;
    [HideInInspector] public float SandOwned;
    [HideInInspector] public float SandBanked;
    public HUDResourceDisplayer WoodDisplayer;
    public HUDResourceDisplayer StoneDisplayer;
    public HUDResourceDisplayer SandDisplayer;

    [Header("Death")]
    public float DeathTime = 6f;
    public PPFactor DeathPostProcessing;
    public StringMorpher DeathStringMorpher;
    public DeathStrings DeathStrings;
    public CanvasGroup DeathCanvasGroup;
    public AnimationCurve DeathCoverOpacity;
    public TextMeshProUGUI RespawnCountdownText;
    public AnimationCurve DeathHealthRestore;
    [HideInInspector] public bool IsDead = false;
    private float deathTimer = 0f;
    private float DeathProgress => deathTimer / DeathTime;
    public SoundCall DeathSound;

    // Gun Selection
    public List<GameObject> GunsOwned;
    private int CurGunIdx = 0;
    
    private void Awake() {
        CurHP = MaxHP;
    }
    
    private void Update() {
        
//        if (photonView.IsMine == false && PhotonNetwork.IsConnected) {    // If this isn't the body the active player is controlling, do nothing.
//            return;
//        }

        if (Take5Damage) {
            Take5Damage = false;
            TakeDamage(5f);
        }
        
        HealthBar.SetProgress(CurHP / MaxHP);
        
        // Shake Bar to show Low Health
        foreach (FxPackageController fx in DamageFX) {
            fx.TriggerAll(BarShakeByHealthLeft.Evaluate(CurHP / MaxHP));
        }
        Vignette.color = new Color(1f, 1f, 1f, BarShakeByHealthLeft.Evaluate(CurHP / MaxHP) * 10f);

        if (!IsDead && CurHP <= 0f) {
            Die();
        }
        
        // Process Death
        DeathCanvasGroup.alpha = !IsDead ? 0f : DeathCoverOpacity.Evaluate(DeathProgress);
        if (DeathProgress > 0.95f && Body == null) {
            SpawnBody();
        }
        if (IsDead) {
            deathTimer += Time.deltaTime;
            SetRespawnCountdown();
            CurHP = MaxHP * DeathHealthRestore.Evaluate(DeathProgress);
            if (DeathProgress >= 1f) {
                IsDead = false;
            }
        }
        
        // Deposit Resources
        if (DepositerIsNearby()) {
            float woodTransfered = Mathf.Clamp(WoodOwned, 0f, 5f * Time.deltaTime);
            WoodOwned -= woodTransfered;
            WoodBanked += woodTransfered;
            
            float stoneTransfered = Mathf.Clamp(StoneOwned, 0f, 5f * Time.deltaTime);
            StoneOwned -= stoneTransfered;
            StoneBanked += stoneTransfered;
            
            float sandTransfered = Mathf.Clamp(SandOwned, 0f, 5f * Time.deltaTime);
            SandOwned -= sandTransfered;
            SandBanked += sandTransfered;
        }
        
        // Set Resources
        WoodDisplayer.SetValue(WoodOwned, WoodBanked);
        StoneDisplayer.SetValue(StoneOwned, StoneBanked);
        SandDisplayer.SetValue(SandOwned, SandBanked);

    }

    public void AddCrystals(Collectible.CollectibleType type, float amount) {
        switch (type) {
            case Collectible.CollectibleType.Wood:
                WoodOwned = Mathf.Clamp(WoodOwned + amount, 0f, ResourceCap);
                break;
            case Collectible.CollectibleType.Stone:
                StoneOwned = Mathf.Clamp(StoneOwned + amount, 0f, ResourceCap);
                break;
            case Collectible.CollectibleType.Sand:
                SandOwned = Mathf.Clamp(SandOwned + amount, 0f, ResourceCap);
                break;
        }
    }

    public void DepositOverTime(float value) {
        crystalsToDeposit += value;
        if (SandOwned < 1) return;
        if (!(crystalsToDeposit >= 1f)) return;
        SandOwned -= (int) crystalsToDeposit;
        crystalsToDeposit -= (int) crystalsToDeposit;
    }

    public void TakeDamage(float amount) {
        if (amount > 0f) {
            CurHP = Mathf.Clamp(CurHP - amount, 0f, MaxHP);
            foreach (FxPackageController fx in DamageFX) {
                fx.TriggerAll(amount / MaxHP * 5f);
            }
        }
    }

    public void TestDeath() {
        CurHP = 0f;
    }

    private void Die() {
        IsDead = true;
        SoundManager.thisSoundManager.PlaySound(DeathSound, SoundManager.instance.gameObject);
        PPManager.Instance.AddFactor(DeathPostProcessing);
        DeathStringMorpher.Clear();
        DeathStringMorpher.StartMorph(DeathStrings.Strings.RandomElement());
        CameraRig.transform.SetParent(null);    // Extract out the camera rig but kill the body.
        Destroy(Body);
    }

    private void SetRespawnCountdown() {
        float timeLeft = DeathTime - deathTimer;
        int secondsLeft = (int) timeLeft;
        float mod = timeLeft % 1f;
        int elipsisDots = (int) ((1f - mod) * 5f);
        StringBuilder sb = new StringBuilder(secondsLeft.ToString());
        for (int i = 0; i < elipsisDots; i++) {
            sb.Append(".");
        }
        RespawnCountdownText.text = sb.ToString();
    }

    private void SpawnBody() {
        deathTimer = 0f;
        IsDead = false;
        if (CameraRig != null) Destroy(CameraRig);
        PlayerObject = GameManager.Instance.SpawnPlayer();
        Body = PlayerObject.GetComponent<PlayerBody>();
        CameraRig = Body.transform.GetComponentInChildren<Camera>().transform.parent.gameObject;
    }

    private bool DepositerIsNearby() {
        if (Body == null) return false;
        foreach (Depositer d in DepositerManager.Instance.Depositers) {
            if (Vector3.Distance(d.transform.position, Body.transform.position) < 5f) {
                return true;
            }
        }

        return false;
    }

    public Gun SetupFirstGun() {
        GameObject newGun = Instantiate(GunsOwned[0], GunAttachPoint.transform.position, GunAttachPoint.transform.rotation, GunAttachPoint);
        return newGun.GetComponent<Gun>();
    }

    public void SwapGun (int dir) {
        int oldGunIdx = CurGunIdx;
        CurGunIdx = (CurGunIdx + dir + GunsOwned.Count) % GunsOwned.Count;
        if (oldGunIdx != CurGunIdx) {
            Body.ActiveGun.PutAway(dir);
        }

        GameObject newGun = Instantiate(GunsOwned[CurGunIdx], GunAttachPoint.transform.position, GunAttachPoint.transform.rotation, GunAttachPoint);
        newGun.transform.localScale = Vector3.one;
        Gun gunController = newGun.GetComponent<Gun>();
        gunController.PlayerController = this;
        Body.ActiveGun = gunController;
        gunController.PullOut(dir);

    }

 }

#if UNITY_EDITOR
 [CustomEditor (typeof(PlayerController))]
 public class PlayerControllerEditor : Editor {
     public override void OnInspectorGUI() {

         base.OnInspectorGUI();
         PlayerController script = (PlayerController)target;
        
         if (GUILayout.Button("Test Death")) {
             script.TestDeath();
         }
     }
 }
#endif