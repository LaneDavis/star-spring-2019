﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUDResourceDisplayer : MonoBehaviour {
    
    public TextMeshProUGUI Text;
    public TextMeshProUGUI BankedText;
    public List<FxPackageController> CollectionFX = new List<FxPackageController>();
    private int lastIntValue;
    private int lastBankedValue;
    
    public void SetValue (float newValue, float newBankedValue) {
        int newIntValue = (int) newValue;
        Text.text = newIntValue.ToString();
        if (newIntValue > lastIntValue) PlayCollectionFX();
        lastIntValue = newIntValue;
        int newBankedIntValue = (int) newBankedValue;
        BankedText.text = newBankedIntValue.ToString();
        if (newBankedIntValue > lastBankedValue) PlayCollectionFX();
        lastBankedValue = newBankedIntValue;
    }

    public void PlayCollectionFX() {
        CollectionFX.TriggerAll();
    }
    
}
