﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;
    public GameObject PlayerPrefab;
    public GameObject CameraRig;

    [Header("Spawning Zone")]
    public float RadiusMin = 125f;
    public float RadiusMax = 175f;
    public Transform PlayerSpawnCenter;

    public Transform CanvasTransform;
    public GameObject PlayerControllerPrefab;
    [HideInInspector] public PlayerController LocalPlayerController;
    
    private void Awake() {
        Instance = this;
    }

    public void AddBodyToGame (GameObject newPlayerBody) {

        if (LocalPlayerController == null) {
            GameObject newPlayerController = Instantiate(PlayerControllerPrefab, CanvasTransform);
           
            newPlayerController.transform.SetParent(CanvasTransform);
            newPlayerController.transform.position = CanvasTransform.position;
            newPlayerController.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
            LocalPlayerController = newPlayerController.GetComponent<PlayerController>();
        }
        
        float magnitude = Random.Range(RadiusMin, RadiusMax);
        Vector2 circlePoint = Random.insideUnitCircle.normalized * magnitude;
        Vector3 spawnPoint = new Vector3(circlePoint.x, 30f, circlePoint.y);

        newPlayerBody.transform.position = PlayerSpawnCenter == null ? spawnPoint : PlayerSpawnCenter.position + spawnPoint;
        GameObject newCamera = Instantiate(CameraRig, newPlayerBody.transform);
        LocalPlayerController.GunAttachPoint = newCamera.GetComponentInChildren<GunAttachPoint>().transform;
        newCamera.GetComponentInChildren<CameraOrbit>().Init(newPlayerBody, LocalPlayerController);
        LocalPlayerController.CameraRig = newCamera;
        Gun gun = LocalPlayerController.SetupFirstGun();
        PlayerBody body = newPlayerBody.GetComponent<PlayerBody>();
        gun.PlayerController = LocalPlayerController;
        body.PlayerController = LocalPlayerController;
        body.ActiveGun = gun;
        LocalPlayerController.Body = body;
        
        // Set the local player to be layer 14, which is invisible to the camera.
        body.gameObject.layer = 14;
        BodyMarker b = body.GetComponentInChildren<BodyMarker>();
        b.gameObject.layer = 14;
        foreach (Transform t in b.gameObject.transform) {
            t.gameObject.layer = 14;
        }
    }

}