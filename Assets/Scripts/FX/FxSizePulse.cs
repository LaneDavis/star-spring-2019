﻿using UnityEngine;

public class FxSizePulse : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 15F)] public float DecayRate = 3F;
	[Range(-5F, 5F)] public float ValueMultiplier = 0.6F;
	public AnimationCurve ValueCurve = new AnimationCurve (new Keyframe (0F, 1F), new Keyframe (1F, 1F));
	[Header("Wave Attributes")]
	public AnimationCurve EaseInCurve = new AnimationCurve (new Keyframe (0F, 0F, 4F, 4F), new Keyframe (0.45F, 1F, 0F, 0F));
	[Range(0F, 10F)] public float WaveFrequency = 1F;

	[Header("Inversion (Squash & Stretch)")]
	public bool InvertX;
	
	//ANATOMY
	private FxApplierSizePulse applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
 
		if (newAnchor.GetComponent<FxApplierSizePulse>() != null) {
			applier = newAnchor.GetComponent<FxApplierSizePulse>();
		} else {
			applier = newAnchor.AddComponent<FxApplierSizePulse>();
		}
		applier.SetUp(newAnchor);

	}

	private void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
			applier.SetPulseFactor(SfxId, ValueCurve.Evaluate(Controller.Timer) * ValueMultiplier * lastPower, DecayRate, EaseInCurve, WaveFrequency, InvertX);
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetPulseFactor(SfxId, ValueCurve.Evaluate(0F) * ValueMultiplier * power, DecayRate, EaseInCurve, WaveFrequency, InvertX);
	}
}
