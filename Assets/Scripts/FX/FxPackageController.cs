﻿using System.Collections.Generic;
using UnityEngine;

public class FxPackageController : FxPackage {

	[Header("Anatomy")]
	public GameObject PackageAnchor;

	[Header("Auto-Play")]
	public bool TriggerOnStart;
	public bool ToggleOnStart;

	[Header("Toggle Settings")]
	public bool AutoOff;
	[Range(0F, 20F)] public float AutoOffTime = 1F;
	public bool AutoKill;
	[Range(0F, 20F)] public float AutoKillTime = 4F;
	[HideInInspector] public float Timer;
	private float timeScale = 1F;

	[Header("Test Buttons")]
	public bool TriggerTest;
	public bool ToggleTest;
	public bool RefreshSetupTest;

	private readonly List<FxPackage> fxList = new List<FxPackage> ();
	
	// ReSharper disable once InconsistentNaming, because this captures deltaTime.
	[HideInInspector] public float dT;

	private void Start () {
		
		if (PackageAnchor == null && transform.parent != null)
			PackageAnchor = transform.parent.gameObject;

		foreach (FxPackage sfx in GetComponents<FxPackage>()) {
			sfx.Controller = this;
			if (sfx == this) continue;
			fxList.Add(sfx);
			sfx.SetUp(PackageAnchor);
		}

		if (TriggerOnStart)
			Trigger();
		if (ToggleOnStart)
			Toggle(true);

	}

	private void Update () {
		
		dT = Time.deltaTime * timeScale;	//Setting this here so constituent SFX components don't have to calculate it independently.
		Timer += dT;
		if (AutoKill && Timer > AutoKillTime)
			Destroy(gameObject);

		if (ToggleState) {
			if (AutoOff && Timer > AutoOffTime)
				ToggleAll(false);
		}

		//Test Features
		if (TriggerTest) {
			TriggerTest = false;
			TriggerAll(1f);
		}

		if (ToggleTest) {
			ToggleTest = false;
			ToggleAll(!ToggleState);
		}

		if (RefreshSetupTest) {
			RefreshSetup();
		}

	}

	public void ToggleAll (bool toggleState, GameObject newAnchor = null, float power = 1f) {

		if (!ToggleState && toggleState) {	
			Timer = 0F;	//Reset timer if appropriate
		}
		ToggleState = toggleState;
		if (newAnchor != null)
			PackageAnchor = newAnchor;
		foreach (FxPackage sfx in fxList)
			sfx.Toggle(toggleState, Anchor, power);
		
	}

	public void TriggerAll (GameObject newAnchor = null, float power = 1f) {

		if (newAnchor != null)
			PackageAnchor = newAnchor;
		foreach (FxPackage sfx in fxList)
			sfx.Trigger(Anchor, power);

	}

	public void TriggerAll(float power = 1f) {
		TriggerAll(null, power:power);
	}

	public void SetTimeScale (float newTimeScale) {
		timeScale = newTimeScale;
	}

	private void RefreshSetup () {
		fxList.Clear();
		foreach (FxPackage sfx in GetComponents<FxPackage>()) {
			sfx.Controller = this;
			if (sfx == this) continue;
			fxList.Add(sfx);
			sfx.SetUp(PackageAnchor);
		}
	}

}
