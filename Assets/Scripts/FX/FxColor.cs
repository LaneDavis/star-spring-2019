﻿using UnityEngine;

public class FxColor : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 15F)] public float DecayRate = 3F;
	[Range(0F, 25F)] public float Strength = 1F;
	public Color Color = Color.black;

	//ANATOMY
	private FxApplierColor applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
 
		if (newAnchor.GetComponent<FxApplierColor>() != null) {
			applier = newAnchor.GetComponent<FxApplierColor>();
		} else {
			applier = newAnchor.AddComponent<FxApplierColor>();
			applier.SetUp(newAnchor);
		}

	}

	private void Update () {

		//If toggled on, set the color value based on a gradient.
		if (ToggleState) {
			Debug.LogWarning("Toggled application is not implemented for FxColor");
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.AddColorFactor(SfxId, Strength * power, DecayRate, Color);
	}
}
