﻿using UnityEngine;

/// <summary>
/// Tracks scaling factors applied to an object, sums them, and sets the object's scale.
/// </summary>
public class FxApplierSizePulse : FxApplier {
	
	private GameObject anchor;

	[HideInInspector] public Vector3 BaseScale = Vector3.zero;

	public void SetUp (GameObject newAnchor = null) {
		if (newAnchor == null) return;
		anchor = newAnchor;
		if (ActiveFactors.Count == 0) {
			BaseScale = newAnchor.transform.localScale;
		}
	}

	public void SetPulseFactor (int sfxId, float newValue, float newDecayRate, AnimationCurve newEaseInCurve, float newWaveFrequency, bool newInvertX) {

		FxFactorSizePulse factor = (FxFactorSizePulse)FactorWithId(sfxId);
		if(factor != null) {
			factor.Value = newValue;
			factor.ResetWaveTimer();
		} else {
			factor = new FxFactorSizePulse ();
			factor.SfxId = sfxId;
			factor.Value = newValue;
			factor.DecayRate = newDecayRate;
			factor.EaseInCurve = newEaseInCurve;
			factor.WaveFrequency = newWaveFrequency;
			factor.InvertX = newInvertX;
			ActiveFactors.Add(factor);
		}

	}

	private void Update () {

		ApplyPulse();
		AgeFactors();

	}

	protected void ApplyPulse () {

		if (anchor != null) {
			Vector3 vec = VectorSum();
			vec = new Vector3(vec.x + 1, vec.y + 1, vec.z + 1);
			anchor.transform.localScale = new Vector3(BaseScale.x * vec.x, BaseScale.y * vec.y, BaseScale.z * vec.z);
		}

	}
	
	private Vector3 VectorSum () {
		Vector3 ret = Vector3.zero;
		foreach (FxFactor factor in ActiveFactors) {
			Vector3 fVec = ((FxFactorSizePulse) factor).ProcessedVector();
			ret = ret + fVec;
		}
		return ret;
	}

}

public class FxFactorSizePulse : FxFactor {

	public AnimationCurve EaseInCurve;
	public float WaveFrequency = 1F;
	protected float WaveTimer;

	public bool InvertX;

	public override void UpdateFactor () {

		base.UpdateFactor();
		WaveTimer += Time.deltaTime;

	}

	public Vector3 ProcessedVector () {

		float val = Value * EaseInCurve.Evaluate(Age) * Mathf.Cos(WaveTimer * WaveFrequency * Mathf.PI * 2F);
		return new Vector3(val * (InvertX ? -1 : 1), val, val);

	}

	public void ResetWaveTimer () {

		WaveTimer = 0F;

	}

}
