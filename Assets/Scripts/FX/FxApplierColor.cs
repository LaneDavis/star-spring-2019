﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FxApplierColor : FxApplier {
	private Color baseColor;
	
	private RawImage rawImageTarget;
	private Image imageTarget;
	private SpriteRenderer spriteTarget;
	private TextMeshProUGUI textTarget;

	private readonly List<ColorWeight> factorWeights = new List<ColorWeight>();
	private Color finalColor;
	
	public void SetUp (GameObject newAnchor = null) {
		if (newAnchor == null) return;

		rawImageTarget = GetComponent<RawImage>();
		imageTarget = GetComponent<Image>();
		spriteTarget = GetComponent<SpriteRenderer>();
		textTarget = GetComponent<TextMeshProUGUI>();

		if (rawImageTarget != null) baseColor = rawImageTarget.color;
		else if (imageTarget != null) baseColor = imageTarget.color;
		else if (spriteTarget != null) baseColor = spriteTarget.color;
		else if (textTarget != null) baseColor = textTarget.color;
		
	}

	public void AddColorFactor (int sfxId, float newValue, float newDecayRate, Color newColor) {

		FxFactorColor factor = (FxFactorColor)FactorWithId(sfxId);
		if(factor != null) {
			factor.Value = newValue;
			factor.Color = newColor;
		} else {
			factor = new FxFactorColor {
				SfxId = sfxId,
				Value = newValue,
				DecayRate = newDecayRate
			};
			factor.Color = newColor;
			ActiveFactors.Add(factor);
		}

	}

	private void Update () {

		ApplyColor();
		AgeFactors();

	}

	private void ApplyColor () {

		factorWeights.Clear();
		factorWeights.Add(new ColorWeight(baseColor, 1f));
		foreach (FxFactor factor in ActiveFactors) {
			factorWeights.Add(new ColorWeight(((FxFactorColor) factor).Color, factor.Value));
		}

		finalColor = ColorUtilities.BlendColors(factorWeights);

		if (rawImageTarget != null) rawImageTarget.color = finalColor;
		else if (imageTarget != null) imageTarget.color = finalColor;
		else if (spriteTarget != null) spriteTarget.color = finalColor;
		else if (textTarget != null) textTarget.color = finalColor;
		
	}

	public void SetBaseColor(Color color) {
		baseColor = color;
	}

}

public class FxFactorColor : FxFactor {

	public Color Color;
	
}
