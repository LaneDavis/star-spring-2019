﻿using UnityEngine;

public class FxRotate : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 150F)] public float DecayRate = 3F;
	public AnimationCurve ValueCurve = new AnimationCurve (new Keyframe (0F, 1F), new Keyframe (1F, 1F));

	//ANATOMY
	private FxApplierRotate applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;

		if (newAnchor.GetComponent<FxApplierRotate>() != null) {
			applier = newAnchor.GetComponent<FxApplierRotate>();
		} else {
			applier = newAnchor.AddComponent<FxApplierRotate>();
			applier.SetUp(newAnchor);
		}

	}

	//If triggered, PASS ONCE through the value curve.
	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetRotateFactor(SfxId, power == 1f ? ValueCurve : AnimationCurveUtilities.MultipliedCurve(ValueCurve, power), DecayRate, false);
	}

	//If toggled, PASS AND CONTINUE through the value curve.
	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		base.Toggle(toggleState, newAnchor);
		if (toggleState)
			applier.SetRotateFactor(SfxId, power == 1f ? ValueCurve : AnimationCurveUtilities.MultipliedCurve(ValueCurve, power), DecayRate, true);
		else
			applier.EndFactor(SfxId);
	}

}
