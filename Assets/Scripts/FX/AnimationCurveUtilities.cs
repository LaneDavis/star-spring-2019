﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimationCurveUtilities {

    public static AnimationCurve MultipliedCurve(AnimationCurve inputCurve, float amplitude) {
        Keyframe[] frames = new Keyframe[inputCurve.keys.Length];
        for (int i = 0; i < inputCurve.keys.Length; i++) {
            frames[i] = new Keyframe(inputCurve.keys[i].time, inputCurve.keys[i].value * amplitude, inputCurve.keys[i].inTangent * amplitude, inputCurve.keys[i].outTangent * amplitude);
        }
        return new AnimationCurve(frames);
    }
    
}
