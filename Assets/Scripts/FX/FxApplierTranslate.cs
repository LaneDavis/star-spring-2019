﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Translates an object to an offset of a base position set on awake.
/// </summary>
public class FxApplierTranslate : FxApplier {
	
	private GameObject anchor;

	private readonly List<FxTranslateFactor> translateFactors = new List<FxTranslateFactor> ();
	private readonly List<FxFactor> shakeFactors = new List<FxFactor> ();

	//SMOOTH TRANSLATE
	private Vector3 basePosition = Vector3.zero;
	private Vector2 factorSum = Vector2.zero;
	
	float xPosPerlinSeed;
	float yPosPerlinSeed;

	public void SetUp (GameObject newAnchor = null) {
		if (newAnchor == null) return;
		anchor = newAnchor;
		basePosition = newAnchor.transform.localPosition;
		
		xPosPerlinSeed = Random.Range(0f, 50f);
		yPosPerlinSeed = Random.Range(100f, 150f);
	}

	public void SetTranslateFactor (int sfxId, Vector2 directionVector, AnimationCurve valueCurve, float newDecayRate, bool loops) {

		FxTranslateFactor factor = (FxTranslateFactor)FactorWithId(sfxId);
		if(factor != null) {
			factor.DirectionVector = directionVector.normalized;
			factor.ValueCurve = valueCurve;
		} else {
			factor = new FxTranslateFactor (sfxId, valueCurve, directionVector.normalized, newDecayRate, loops);
			translateFactors.Add(factor);
		}

	}

	public void SetShakeFactor (int sfxId, float newValue, float perlinSpeed, float newDecayRate) {

		FxFactor factor = FactorWithId(sfxId);
		if(factor != null) {
			factor.Value = newValue;
		} else {
			factor = new FxFactor {
				SfxId = sfxId,
				Value = newValue,
				Speed = perlinSpeed,
				DecayRate = newDecayRate
			};
			shakeFactors.Add(factor);
		}

	}

	private void Update () {

		AgeFactors();
		ApplyTranslation();

	}

	private void ApplyTranslation () {

		if (anchor != null) {
			anchor.transform.localPosition = basePosition + (Vector3)TranslationSum() + new Vector3(0.5f - Mathf.PerlinNoise(Time.time * ShakePerlinSpeed() * 2f, xPosPerlinSeed), 0.5f - Mathf.PerlinNoise(Time.time * ShakePerlinSpeed() * 2f, yPosPerlinSeed)) * ShakeSum() * 4.5f;
		}

	}

	public Vector2 TranslationSum () {
		factorSum = Vector2.zero;
		foreach (FxTranslateFactor factor in translateFactors) {
			factorSum += factor.LastDisplacement;
		}
		return factorSum;
	}

	public void EndFactor (int sfxId) {
		foreach (FxTranslateFactor factor in translateFactors) {
			if (factor.SfxId == sfxId)
				factor.Deactivate();
		}
	}

	public float ShakeSum () {
		float shakeSum = 0F;
		foreach (FxFactor factor in shakeFactors) {
			shakeSum += factor.Value;
		}
		return shakeSum;
	}

	public float ShakePerlinSpeed() {
		if (shakeFactors.Count == 0) return 0f;
		float perlinSum = 0f;
		foreach (FxFactor factor in shakeFactors) {
			perlinSum += factor.Speed;
		}
		return perlinSum / shakeFactors.Count;
	}

	override protected void AgeFactors () {
		FactorsToDelete.Clear();
		foreach (FxTranslateFactor factor in translateFactors) {
			factor.UpdateFactor();
			if (factor.GetShouldBeDeleted())
				FactorsToDelete.Add(factor);
		}
		foreach (var fxFactor in FactorsToDelete) {
			var factor = (FxTranslateFactor) fxFactor;
			translateFactors.Remove(factor);
		}

		foreach (FxFactor factor in shakeFactors) {
			factor.UpdateFactor();
			if (factor.GetShouldBeDeleted())
				FactorsToDelete.Add(factor);
		}
		foreach (FxFactor factor in FactorsToDelete) {
			shakeFactors.Remove(factor);
		}
	}

	protected override FxFactor FactorWithId (int id) {
		foreach (FxFactor factor in translateFactors) {
			if (factor.SfxId == id)
				return factor;
		}
		foreach (FxFactor factor in shakeFactors) {
			if (factor.SfxId == id)
				return factor;
		}
		return null;
	}

}

public class FxTranslateFactor : FxFactor {

	public AnimationCurve ValueCurve;
	public Vector2 DirectionVector;

	public Vector2 LastDisplacement = Vector2.zero;
	protected readonly bool Loops;
	protected bool IsOn = true;

	public FxTranslateFactor (int sfxId, AnimationCurve valueCurve, Vector2 directionVector, float decayRate, bool loops) {
		SfxId = sfxId;
		ValueCurve = valueCurve;
		DirectionVector = directionVector;
		DecayRate = decayRate;
		Loops = loops;
	}

	public override void UpdateFactor () {
		
		Age += Time.deltaTime;
		if (IsOn) {
			LastDisplacement = DisplacementFromCurve();
		} else {
			LastDisplacement = LastDisplacement.normalized * LastDisplacement.magnitude * Mathf.Exp(-DecayRate * Time.deltaTime);
			if (LastDisplacement.magnitude < DeletionThreshold)
				ShouldBeDeleted = true;
		}

		//Handle LOOP (toggle) vs PASS ONCE (trigger) behavior.
		if (Age > ValueCurve.keys[ValueCurve.keys.Length - 1].time) {
			if (Loops)
				Age = 0F;
			else
				IsOn = false;
		}
		
	}

	public void Deactivate () {
		IsOn = false;
	}

	protected Vector2 DisplacementFromCurve () {
		return DirectionVector * ValueCurve.Evaluate(Age);
	}

}
