﻿using System.Collections.Generic;
using UnityEngine;

public class FxParticles : FxPackage {

	//ATTRIBUTES
	public bool RendersUnderAnchor = true;
	public bool AddsCanvas = true;
	public bool SimpleEmit = true;
	public int SortingOrder = 5000;

	//ANATOMY
	private readonly List<ParticleSystem> systemList = new List<ParticleSystem>();

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
		systemList.Clear();
		foreach (ParticleSystem system in GetComponentsInChildren<ParticleSystem>()) {
			systemList.Add(system);
		}

		//Add canvas component to parent, pushing it to a higher rendering order.
		if (!RendersUnderAnchor) return;
		if (AddsCanvas && newAnchor.GetComponent<Canvas>() == null) {
			newAnchor.AddComponent<Canvas>();
		}
		newAnchor.GetComponent<Canvas>().overrideSorting = true;
		newAnchor.GetComponent<Canvas>().sortingOrder = SortingOrder;

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		if (SimpleEmit) {
			foreach (var t in systemList) {
				t.Emit((int)(t.emission.rateOverTime.Evaluate(0f) * power));
			}
		} else {
			foreach (var t in systemList) {
				t.Play();
			}
		}
	}

	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		if (ToggleState != toggleState){
			base.Toggle(toggleState, newAnchor);
			foreach (var t in systemList){
				if (toggleState)
					t.Play();
				else
					t.Stop();
			}
		}
	}

}
