﻿using UnityEngine;

public class FxLighting : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 15F)] public float DecayRate = 3F;
	public Color OverColor;
	public Color UnderColor;
	public AnimationCurve EaseInCurve = new AnimationCurve (new Keyframe (0F, 0F, 4F, 4F), new Keyframe (0.45F, 1F, 0F, 0F));

	public override void SetUp (GameObject newAnchor = null) {

		//Do Nothing.

	}

	void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
			FxLightingManager.Instance.SetLightingFactor(SfxId, OverColor, UnderColor, DecayRate, EaseInCurve);
		}

	}

	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		if (FxLightingManager.Instance != null)
			FxLightingManager.Instance.SetLightingFactor(SfxId, OverColor, UnderColor, DecayRate, EaseInCurve);
	}
}
