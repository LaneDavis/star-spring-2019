﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShoot : Gun
{

  public LayerMask layerMask;
  public float Dammage = 25f;
  public float fireRate = 1f;
  public float weaponrange = 1000f;
  public float hitForce = 1f;
   
//  public Transform gunEnd;


  private Camera FPScam;
  private WaitForSeconds shotDuration = new WaitForSeconds(.07f);
  private AudioSource gunAudio;
   // private LineRenderer bulletTracers;
private float nextFire;

// Start is called before the first frame update
  void Start()
  {

        //bulletTracers = GetComponent<LineRender>();
    gunAudio = GetComponent<AudioSource>();
      FPScam = GetComponentInParent<Camera>();

  }

     //Update is called once per frame
  

 //private IEnumerator ShotEffect ()
//{

// gunAudio.Play();

//laserLine.enabled = true;
//yield return shotDuration;
//laserLine.enabled = false;



 //}
protected override void Shoot()
{

  RaycastHit Hit;

  Physics.Raycast(FPScam.transform.position, FPScam.transform.forward * weaponrange, out Hit, layerMask);

 DamageReceiver dr =
      Hit.collider.transform.root.GetComponent<DamageReceiver>();


if (dr != null)
{
     dr.TakeDamage(10f);
}


}


}
