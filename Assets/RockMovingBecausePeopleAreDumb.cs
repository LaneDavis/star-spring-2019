﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockMovingBecausePeopleAreDumb : MonoBehaviour {

    public float MaxHeight;

    private Vector3 basePosition;
    public float BobTime;
    private float bobTimer;
    
    private void Start() {
        basePosition = transform.position;
        bobTimer = Random.Range(0f, BobTime * Mathf.PI * 2f);
    }

    private void Update() {
        bobTimer += Time.deltaTime;
        transform.position = basePosition + Vector3.up * MaxHeight * (Mathf.Sin(bobTimer / BobTime) + 1f);
    }

}
