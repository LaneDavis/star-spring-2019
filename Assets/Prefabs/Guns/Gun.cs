﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    [HideInInspector] public PlayerController PlayerController;
    
    public GameObject bulletPrefab;
    public float cooldown = .1f;
    private float timer;

    public SoundCall ShootSound;

    [Header("Pull Out Animation")]
    public AnimationCurve PullOutScaleCurve;
    public AnimationCurve PullOutRotationCurve;
    private float pullOutTimer = 1f;
    private float pullOutDir = 1f;
    
    [Header("Put Away Animation")]
    public AnimationCurve PutAwayScaleCurve;
    public AnimationCurve PutAwayRotationCurve;
    private float putAwayTimer = 0f;
    private bool isPuttingAway = false;
    private float putAwayDir = 1f;

    private bool isFirstUpdate = true;

    [Header("Bob")]
    public AnimationCurve BobHeightCurve;
    public AnimationCurve BobRotationCurve;
    private Vector3 basePosition;
    private float bobTimer;

    private void FirstUpdate() {
        isFirstUpdate = false;
    }
    
    protected virtual void Update() {
        
        if (isFirstUpdate) FirstUpdate();
        
        // Update Shot Timer
        timer += Time.deltaTime;
        
        // Update Pull Out / Put Away Animations
        pullOutTimer += Time.deltaTime;
        if (isPuttingAway) putAwayTimer += Time.deltaTime;
        transform.localScale = Vector3.one * PullOutScaleCurve.Evaluate(pullOutTimer) * PutAwayScaleCurve.Evaluate(putAwayTimer);
        transform.localRotation = Quaternion.identity;
        transform.Rotate(0f, 0f, pullOutDir * PullOutRotationCurve.Evaluate(pullOutTimer) + putAwayDir * PutAwayRotationCurve.Evaluate(putAwayTimer) + BobRotationCurve.Evaluate(bobTimer));
        if (isPuttingAway && putAwayTimer > PutAwayRotationCurve.keys[PutAwayRotationCurve.keys.Length - 1].time) {
            Destroy(gameObject);
        }

        transform.localPosition = Vector3.zero + Vector3.up * BobHeightCurve.Evaluate(bobTimer);

    }

    public void TryShoot() {
        if (timer > cooldown) {
            timer = 0f;
            Shoot();
        }
    }

    protected virtual void Shoot() {
        
        // Override this! Each gun has its own style of shooting.
        
    }

    public void PullOut(int dir) {
        pullOutTimer = 0f;
        pullOutDir = dir;
    }
    
    public void PutAway (int dir) {
        isPuttingAway = true;
        putAwayDir = dir;
    }

    public void ApplyBob(float bob) {
        bobTimer += Time.deltaTime * bob;
    }
    
}
