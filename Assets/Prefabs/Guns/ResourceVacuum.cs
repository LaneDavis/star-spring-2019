﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ResourceVacuum : Gun {

    [Header("Resource Pull")]
    public LayerMask RaycastMask;
    public float PullRadius;
    public float PullRange;
    public float PullSpeed;
    public int PullCount = 10;
    public float PullDuration = 0.2f;
    private float pullTimeLeft = 0f;
    
    protected override void Shoot() {
        pullTimeLeft = PullDuration;
    }

    protected override void Update() {
        base.Update();

        if (pullTimeLeft > 0f) {
            pullTimeLeft -= Time.deltaTime;
            for (int i = 0; i < PullCount; i++) {
                RaycastHit hit;
                Physics.Raycast(PlayerController.CameraRig.transform.position + new Vector3(0f, Random.insideUnitCircle.y, Random.insideUnitCircle.x) * PullRadius + transform.forward, transform.right, out hit, PullRange, RaycastMask);
                if (hit.collider != null) {
                    Collectible c = hit.collider.GetComponent<Collectible>();
                    if (c != null) {
                        c.ApplyPull(transform.position, PullSpeed);
                    }
                }
            }
        }
    }
    
}
