﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : Gun {
    
    protected override void Shoot() {
        GameObject newBullet = Instantiate(bulletPrefab);
        newBullet.SetActive(true);
        newBullet.transform.position = transform.position;
        newBullet.transform.rotation = transform.rotation;
        newBullet.transform.RotateAround(newBullet.transform.position, Vector3.up, 0f);
        newBullet.GetComponent<Rigidbody>().velocity = transform.right * 50f;
        newBullet.GetComponent<Rocket>().PlayerController = PlayerController;
        if (ShootSound != null) {
            SoundManager.instance.PlaySound(ShootSound, gameObject);
        }
    }
    
}
