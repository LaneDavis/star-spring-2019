﻿// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

using UnityEngine;

[AddComponentMenu("")]
public class Rocket : MonoBehaviour {

    [HideInInspector] public PlayerController PlayerController;
    
    public float lifeTime = 3.0f;
    private bool die;
    private float deathTime;
    private float startTime;
    private float LifeProgress => (Time.time - startTime) / (deathTime - startTime);
    public AnimationCurve ScaleCurve;
    private Vector3 baseScale;
    public Transform TrailParticles;
    private float trailParticleTimer;
    public float TrailParticleDelay = 0.25f;
    private bool trailParticlesActivated;

    public GameObject ExplosionPrefab;
    public GameObject ExplosionFXPrefab;

    void Awake() {
        baseScale = transform.localScale;
        transform.localScale = Vector3.zero;
    }
    
    void Start() {
        if(lifeTime > 0.0f) {
            startTime = Time.time;
            deathTime = Time.time + lifeTime; 
            die = true;
        }
    }

    void Update() {

        if (!trailParticlesActivated) {
            trailParticleTimer++;
            if (trailParticleTimer > TrailParticleDelay) {
                trailParticlesActivated = true;
                TrailParticles.GetComponent<ParticleSystem>().Play();
            }
        }
        
        if (die && Time.time >= deathTime) {
            TrailParticles.SetParent(null);
            Destroy(gameObject);
        }
        transform.localScale = baseScale * ScaleCurve.Evaluate(LifeProgress);
    }
    
    public void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == 10) {
            DamagePlayersInArea();
            TrailParticles.SetParent(null);
            Destroy(gameObject);
        }
    }
    
    public void OnCollisionEnter (Collision other) {
        if (other.gameObject.layer == 10 || other.gameObject.layer >= 12 && other.gameObject != PlayerController.PlayerObject) {
            Instantiate(ExplosionFXPrefab, transform.position, Quaternion.identity, null);
            Instantiate(ExplosionPrefab, transform.position, Quaternion.identity, null);
            TrailParticles.SetParent(null);
            Destroy(gameObject);
        }
    }

    private void DamagePlayersInArea() {
//        foreach (PlayerController pc in PlayerManager.Instance.PlayerControllers) {
//            if (pc.Body != null) {
//                Vector3 dif = transform.position - pc.Body.transform.position;
//                pc.Body.TakeDamage(DamageAmount * DamageFallOffByRadius.Evaluate(dif.magnitude / DamageRadius));
//                pc.Body.rb.AddExplosionForce(1000f, transform.position - Vector3.up, 10f);
//            }
//        }
    }
    
}
