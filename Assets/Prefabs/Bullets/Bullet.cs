﻿// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

namespace Rewired.Demos {

    using UnityEngine;

    [AddComponentMenu("")]
    public class Bullet : MonoBehaviour {

        public float lifeTime = 3.0f;
        private bool die;
        private float deathTime;
        private float startTime;
        private float LifeProgress => (Time.time - startTime) / (deathTime - startTime);
        public AnimationCurve ScaleCurve;
        private Vector3 baseScale;

        void Start() {
            baseScale = transform.localScale;
            if(lifeTime > 0.0f) {
                startTime = Time.time;
                deathTime = Time.time + lifeTime;
                die = true;
            }
        }

        void Update() {
            if(die && Time.time >= deathTime) Destroy(gameObject);
            transform.localScale = baseScale * ScaleCurve.Evaluate(LifeProgress);
        }
    }
}
