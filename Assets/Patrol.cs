﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
  
  public float speed;
   public Transform moveSpot;
   
    private float waitTime;
    public float startWaitTime;
    public float minX;
    public float maxX;
    public float maxZ;
    public float minZ;
    public float maxY;
    public float minY;

    void Start()
    {

        waitTime = startWaitTime;
        moveSpot.position = new Vector3(Random.Range(minX, maxX), Random.Range(minZ, maxZ), Random.Range(minY, maxY));

    }
     void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, moveSpot.position, speed * Time.deltaTime);

        


        if(Vector3.Distance(transform.position, moveSpot.position) < 0.2f)
        {
            if(waitTime <= 0)
            {
                
                waitTime = startWaitTime;
            } else
            {
                waitTime -= Time.deltaTime;
            }



        }
    }


}
