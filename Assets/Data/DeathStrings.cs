﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Death Strings")]
public class DeathStrings : ScriptableObject {

    public List<string> Strings = new List<string>();

}