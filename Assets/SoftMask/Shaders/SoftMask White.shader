﻿Shader "Sprites/White"
{

    Properties
    {
        [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        _Color("Tint", Color) = (1,1,1,1)

        [PerRendererData] _SoftMask("Mask", 2D) = "white" {}

        _StencilComp("Stencil Comparison", Float) = 8
        _Stencil("Stencil ID", Float) = 0
        _StencilOp("Stencil Operation", Float) = 0
        _StencilWriteMask("Stencil Write Mask", Float) = 255
        _StencilReadMask("Stencil Read Mask", Float) = 255

        _ColorMask("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }

        Stencil
        {
            Ref[_Stencil]
            Comp[_StencilComp]
            Pass[_StencilOp]
            ReadMask[_StencilReadMask]
            WriteMask[_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest[unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask[_ColorMask]

        Pass
        {
            Name "Default"
       CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
            #include "SoftMask.cginc"
            
             #pragma target 2.0

        #if UNITY_VERSION >= 201720
            #pragma multi_compile __ UNITY_UI_CLIP_RECT
        #endif
            #pragma multi_compile __ UNITY_UI_ALPHACLIP
            #pragma multi_compile __ SOFTMASK_SIMPLE SOFTMASK_SLICED SOFTMASK_TILED
 
            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0;
              #if UNITY_VERSION >= 550
                      UNITY_VERTEX_INPUT_INSTANCE_ID
              #endif
            };
            
            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
        #if UNITY_VERSION >= 550
                UNITY_VERTEX_OUTPUT_STEREO
        #endif
                SOFTMASK_COORDS(2)
            };
            
            fixed4 _Color;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;
        
            sampler2D _MainTex;
        #ifdef SOFTMASK_ETC1
            sampler2D _AlphaTex;
        #endif
        #if UNITY_VERSION >= 201800
            float4 _MainTex_ST;
        #endif
 
            v2f vert(appdata_t IN)
            {
                v2f OUT;
        #if UNITY_VERSION >= 550
                UNITY_SETUP_INSTANCE_ID(IN);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
        #endif
                OUT.worldPosition = IN.vertex;
        #if UNITY_VERSION >= 540
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
        #else
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
        #endif
        
        #if UNITY_VERSION >= 201800
                OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
        #else
                OUT.texcoord = IN.texcoord;
        #endif
        
        #if UNITY_VERSION < 550
        #  ifdef UNITY_HALF_TEXEL_OFFSET
                OUT.vertex.xy += (_ScreenParams.zw - 1.0) * float2(-1, 1);
        #  endif
        #endif
        
                OUT.color = IN.color * _Color;
                SOFTMASK_CALCULATE_COORDS(OUT, IN.vertex)
                return OUT;
        }
 
            uniform float _EffectAmount;
 
            fixed4 frag(v2f IN) : COLOR
            {
            
                #ifdef SOFTMASK_ETC1
                half4 color = UnityGetUIDiffuseColor(IN.texcoord, _MainTex, _AlphaTex, _TextureSampleAdd) * IN.color;
        #else
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;
        #endif
        
                color.a *= SOFTMASK_GET_MASK(IN);
        
        #if defined(UNITY_UI_CLIP_RECT) || UNITY_VERSION < 201720
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
        #endif
        
        #if defined(UNITY_UI_ALPHACLIP)
                clip(color.a - 0.001);
        #endif
                       
                color.rgb = float3(1, 1, 1);
                return color;
            }
        ENDCG
        }
    }
}

// UNITY_SHADER_NO_UPGRADE
